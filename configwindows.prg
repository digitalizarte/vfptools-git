Local lcWinName As String, ;
	liIdx As interger, ;
	llOk  As Boolean, ;
	lnFormCount As Integer, ;
	loForm As Form


* ConfigWindows
* @Date 2014-03-22
* @Author DAE - digitalizarte@gmail.com
*

* Set Library To WSIZE Additive

* Abre la ventana de commandos
lcWinName = 'Command'
If ! Wexist( lcWinName )
* Keyboard '{CTRL+F2}' Plain
	Sys(1500, '_MWI_CMD', '_MSM_WINDO')

Endif && ! Wexist( lcWinName )
Dock Window ( lcWinName ) Position 3
Activate Window ( lcWinName )

* Abre la ventana de propiedades
lcWinName = 'Properties'
If ! Wexist( lcWinName )
	Sys(1500, '_MWI_PROPERTIES', '_MSM_WINDO')

Endif && ! Wexist( lcWinName )
Dock Window ( lcWinName ) Position 2
Activate Window ( lcWinName )

* Abre la ventana de Document View
lcWinName = 'Document View'
If ! Wexist( lcWinName )
	Sys(1500, '_MTI_DOCVIEW', '_MSM_TOOLS')

Endif && ! Wexist( lcWinName )
Dock Window ( lcWinName ) Position 4 Window 'Properties'
Activate Window ( lcWinName )


liIdx = 1
llOk = .F.
lnFormCount = _Screen.FormCount
Do While !llOk  And liIdx <= lnFormCount
	loForm = _Screen.Forms[ liIdx ]
	llOk = loForm.Name == 'frmPaneManager'
	If llOk
		loForm.Move( 0, 0, 500, _screen.Height / 1.085 )
		Activate Window ( loForm.Name )

	Else
		liIdx = liIdx + 1

	Endif

Enddo
